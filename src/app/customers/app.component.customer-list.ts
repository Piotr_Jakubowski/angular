import { Component, OnInit } from '@angular/core';
import { Customer } from './app.customer';
import { CustomerService } from './app.customer.service';

@Component({
    selector: 'my-tab',
    providers: [CustomerService],
    templateUrl: './customer_list.html'
})
export class CustomerListComponent implements OnInit {

    constructor(private customerService: CustomerService) { }

    customers: Customer[];

    ngOnInit() {
        this.customers = this.customerService.getAllCustomers();
    }
}