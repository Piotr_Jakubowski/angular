import { Injectable } from '@angular/core';

import { Customer } from './app.customer';

@Injectable()
export class CustomerService {

    private customers = [
        new Customer(1, 'Marian', 'Mariański'),
        new Customer(2, 'Szymon', 'Szymański'),
        new Customer(3, 'Aleksandra', 'Michniewicz'),
        new Customer(4, 'Rozalia', 'Żołądź')
    ];

    getAllCustomers() {
        return this.customers;
    }
}