import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class TitleFilterPipe implements PipeTransform {

    transform(items: any, value: any) {
        if (value === undefined) { return items; }

        return items.filter((item: any) => {
            return item.title.toLowerCase().includes(value.toLowerCase());
        })
    }
}