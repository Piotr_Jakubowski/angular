import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { Movie } from '../movie/details/app.movie';
import { Status } from '../movie/details/app.movie.status';
import { MoviesData } from './data/app.mock.movies-data';

@Injectable()
export class MovieService {
    constructor(private http: Http) {
        this.http = http;
    }

    private apiUrl = 'http://www.omdbapi.com';

    getCategories() {
        return MoviesData.getCategories();
    }

    getFreeMovies() {
        return MoviesData.getFreeMovies();
    }

    getRentedMovies() {
        return MoviesData.getRentedMovies();
    }

    add(movie: Movie) {
        return MoviesData.addMovie(movie);
    }

    edit(movie: Movie) {
        return MoviesData.editMovie(movie);
    }

    rent(id: number, rent_date: Date) {
        return MoviesData.rentMovie(id, rent_date);
    }

    return(id: number) {
        return MoviesData.returnMovie(id);
    }

    getPosterUrl(title: string) {
        let params: URLSearchParams = new URLSearchParams();
        params.set("t", title);
        return this.http.get(this.apiUrl, { search: params })
            .map(this.extractPosterUrlData);
    }

    private extractPosterUrlData(res: Response) {
        let body = res.json();
        return body.Poster || {};
    }

}