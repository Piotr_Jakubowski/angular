import { MovieListComponent } from './app.component.movie-list';
import { MovieService } from "./app.movie.service";
import { MovieComponent } from "../movie/details/app.component.movie-details";
import { MovieRentComponent } from "../movie/rent/app.component.movie-rent";
import { MovieAddComponent } from "../movie/add/app.component.movie-add";
import { MovieEditComponent } from "../movie/edit/app.component.movie-edit";
import { Status } from "../movie/details/app.movie.status";
import { Movie } from "../movie/details/app.movie";
import { ArraySortPipe } from "./app.text-sort.pipe";
import { TitleFilterPipe } from "./app.title-filter.pipe";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('MovieListComponent', function () {
    let de: DebugElement;
    let comp: MovieListComponent;
    let fixture: ComponentFixture<MovieListComponent>;
    let movieService: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieListComponent,
                MovieComponent,
                MovieRentComponent,
                MovieAddComponent,
                MovieEditComponent,
                ArraySortPipe,
                TitleFilterPipe
            ],
            imports: [FormsModule, HttpModule],
            providers: [MovieService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieListComponent);
        comp = fixture.componentInstance;
        movieService = fixture.debugElement.injector.get(MovieService);
    });

    it('should assign categories', () => {
        let cat = giveCategories();
        spyOn(movieService, 'getCategories').and.returnValue(Observable.of(cat));
        fixture.detectChanges();

        expect(comp.categories.length).toEqual(cat.length);
    });

    it('should assign movies', () => {
        let mov = giveMovies();
        spyOn(movieService, 'getFreeMovies').and.returnValue(Observable.of(mov));
        fixture.detectChanges();

        expect(comp.movies.length).toEqual(mov.length);
    });

    it('should assign movie to selected movie on select', () => {
        let movie = giveMovies()[0];
        comp.onSelect(movie);
        fixture.detectChanges();

        expect(comp.selectedMovie).toEqual(movie);
    });

    it('should assign poster url to selected movie on select', () => {
        let movie = giveMovies()[0];
        let fakeImgUrl = 'fake_url.com';
        spyOn(movieService, 'getPosterUrl').and.returnValue(Observable.of(fakeImgUrl));
        comp.onSelect(movie);
        fixture.detectChanges();

        expect(comp.selectedMovie.poster_url).toEqual(fakeImgUrl);
    });

    var giveCategories = function () {
        return ['Horror', 'Comedy', 'Drama'];
    }

    var giveMovies = function () {
        let cat = giveCategories();
        return [new Movie(0, 'Focus', 'Christopher Nolan', cat[0], new Date('2010-11-16'), Status.FREE),
        new Movie(1, 'Braveheart', 'Mel Gibson', cat[2], new Date('2012-10-16'), Status.RENTED)]
    }
});