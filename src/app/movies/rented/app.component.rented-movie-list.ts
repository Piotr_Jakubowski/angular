import { Component, OnInit } from '@angular/core';
import { Movie } from '../../movie/details/app.movie';
import { MovieService } from '../app.movie.service';

@Component({
    selector: 'my-tab',
    providers: [MovieService],
    templateUrl: './rented-movie-list.html'
})
export class RentedMovieListComponent implements OnInit {
    constructor(private movieService: MovieService) { }

    categories: string[];
    movies: Movie[];
    selectedMovie: Movie;

    ngOnInit(): void {
        this.movieService.getCategories().subscribe(categories => this.categories = categories);
        this.refreshMovies();
    }

    refreshMovies() {
        this.movieService.getRentedMovies().subscribe(movies => this.movies = movies,
            error => this.movies = []);
    }

    onSelect(movie: Movie): void {
        this.selectedMovie = movie;
        this.getPosterUrl();
    }

    private getPosterUrl() {
        this.movieService.getPosterUrl(this.selectedMovie.title).subscribe(data => this.selectedMovie.poster_url = data.toString(),
            e => console.log(e.toString()),
            () => console.log(this.selectedMovie.title + " image load completed"));
    }
}