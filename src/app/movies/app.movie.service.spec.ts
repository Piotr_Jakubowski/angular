import { MovieService } from "./app.movie.service";
import { Status } from "../movie/details/app.movie.status";
import { Movie } from "../movie/details/app.movie";
import { MoviesData } from "./data/app.mock.movies-data";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {
    BaseRequestOptions,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import {
    MockBackend,
    MockConnection
} from '@angular/http/testing';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('MovieService', function () {
    let backend: MockBackend;
    let service: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                MovieService,
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions
                    ],
                    provide: Http,
                    useFactory: (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
                        return new Http(backend, defaultOptions);
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        const testbed = getTestBed();
        backend = testbed.get(MockBackend);
        service = testbed.get(MovieService);
    });

    it('should return categories', () => {
        let categories = giveCategories();
        spyOn(MoviesData, 'getCategories').and.returnValue(Observable.of(categories));

        expect(Observable.of(categories)).toEqual(service.getCategories());
    });

    it('should return free movies', () => {
        let movie = giveFreeMovies();
        spyOn(MoviesData, 'getFreeMovies').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.getFreeMovies());
    });

    it('should return rented movies', () => {
        let movie = giveRentedMovies();
        spyOn(MoviesData, 'getRentedMovies').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.getRentedMovies());
    });

    it('should return added movie', () => {
        let movie = giveRentedMovies()[0];
        spyOn(MoviesData, 'addMovie').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.add(movie));
    });

    it('should return edited movie', () => {
        let movie = giveRentedMovies()[0];
        spyOn(MoviesData, 'editMovie').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.edit(movie));
    });

    it('should return rented movie', () => {
        let movie = giveFreeMovies()[0];
        spyOn(MoviesData, 'rentMovie').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.rent(movie.id, new Date()));
    });

    it('should return returned movie', () => {
        let movie = giveRentedMovies()[0];
        spyOn(MoviesData, 'returnMovie').and.returnValue(Observable.of(movie));

        expect(Observable.of(movie)).toEqual(service.return(movie.id));
    });

    var giveCategories = function () {
        return ['Horror', 'Comedy', 'Drama'];
    }

    var giveFreeMovies = function () {
        return [new Movie(0, 'Focus', 'Christopher Nolan', giveCategories()[0], new Date('2010-11-16'), Status.FREE)];
    }

    var giveRentedMovies = function () {
        return [new Movie(0, 'Focus', 'Christopher Nolan', giveCategories()[0], new Date('2010-11-16'), Status.RENTED)];
    }
});