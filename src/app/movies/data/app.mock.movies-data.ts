import { Movie } from '../../movie/details/app.movie';
import { Status } from '../../movie/details/app.movie.status';

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

export class MoviesData {
    private static CATEGORIES = ['Horror', 'Comedy', 'Science-Fiction', 'Drama'];

    private static MOVIES = [
        new Movie(0, 'Focus', 'Christopher Nolan', MoviesData.CATEGORIES[0], new Date('2010-11-16'), Status.FREE),
        new Movie(1, 'Braveheart', 'Mel Gibson', MoviesData.CATEGORIES[3], new Date('2012-10-16'), Status.RENTED, new Date()),
        new Movie(2, 'Dark Knight', 'Andrew Maclore', MoviesData.CATEGORIES[2], new Date('2012-10-02'), Status.FREE),
        new Movie(3, 'Incredibles', 'Samuel Sarath', MoviesData.CATEGORIES[3], new Date('2014-09-13'), Status.RENTED, new Date())
    ];
    static getCategories() {
        return Observable.of(this.CATEGORIES);
    }

    static getFreeMovies() {
        return Observable.of(this.MOVIES.filter((movie) => {
            return movie.status === Status.FREE;
        }));
    }

    static getRentedMovies() {
        return Observable.of(this.MOVIES.filter((movie) => {
            return movie.status === Status.RENTED;
        }));
    }

    static addMovie(movie: Movie) {
        movie.id = this.MOVIES.length;
        this.MOVIES.push(movie);
        return Observable.of(movie);
    }

    static editMovie(movie: Movie) {
        for (let i in this.MOVIES) {
            if (this.MOVIES[i].id === movie.id) {
                this.MOVIES[i] = movie;
                return Observable.of(this.MOVIES[i]);
            }
        }
    }

    static rentMovie(id: number, rent_date: Date) {
        for (let i in this.MOVIES) {
            if (this.MOVIES[i].id === id) {
                this.MOVIES[i].rent_date = rent_date;
                this.MOVIES[i].status = Status.RENTED;
                return Observable.of(this.MOVIES[i]);
            }
        }
    }

    static returnMovie(id: number) {
        for (let i in this.MOVIES) {
            if (this.MOVIES[i].id === id) {
                this.MOVIES[i].status = Status.FREE;
                return Observable.of(this.MOVIES[i]);
            }
        }
    }

}