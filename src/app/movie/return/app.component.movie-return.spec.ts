import { MovieReturnComponent } from './app.component.movie-return';
import { MovieService } from "../../movies/app.movie.service";
import { Status } from "../../movie/details/app.movie.status";
import { Movie } from "../../movie/details/app.movie";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, Component } from '@angular/core';

describe('MovieReturnComponent', function () {
    let de: DebugElement;
    let comp: MovieReturnComponent;
    let fixture: ComponentFixture<MovieReturnComponent>;
    let movieService: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieReturnComponent,
            ],
            imports: [FormsModule, HttpModule],
            providers: [MovieService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieReturnComponent);
        comp = fixture.componentInstance;
        comp.movie = giveMovie();
        fixture.detectChanges();
        movieService = fixture.debugElement.injector.get(MovieService);
    });

    it('should return movie', () => {
        spyOn(movieService, 'return');
        // comp.movie = giveMovie();

        comp.returnMovie(1);
        fixture.detectChanges();

        expect(movieService.return).toHaveBeenCalled();
    });

    it('should calculate fee', () => {
        let isBroken = true;
        let isNotRewinded = true;
        let delayedDays = 2;
        let expectedFee = 55;

        let fee = comp.calculateFee(isBroken, isNotRewinded, delayedDays);

        expect(fee).toEqual(expectedFee);
    });

    var giveCategories = function () {
        return ['Horror', 'Comedy', 'Drama'];
    }

    var giveMovie = function () {
        return new Movie(0, 'Focus', 'Christopher Nolan', giveCategories()[0], new Date('2010-11-16'), Status.FREE);
    }
});