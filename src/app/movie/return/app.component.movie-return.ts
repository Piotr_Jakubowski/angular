import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../details/app.movie';
import { MovieService } from '../../movies/app.movie.service';

@Component({
    selector: 'movie-return',
    templateUrl: './movie-return.html',
})
export class MovieReturnComponent {

    constructor(private movieService: MovieService) { }

    @Input() movie: Movie;
    @Output() returnAction = new EventEmitter();

    readonly FEE_DELAY_PER_DAY = 5;
    readonly FEE_TAPE_DESTROYED = 40;
    readonly FEE_TAPE_NOT_REWINDED = 5;

    returnMovie(id: number) {
        this.movieService.return(id);
        this.returnAction.emit();
    }

    calculateFee(broken: boolean, not_rewinded: boolean, delay_days: number) {
        let fee = 0;
        if (broken) { fee += this.FEE_TAPE_DESTROYED }
        if (not_rewinded) { fee += this.FEE_TAPE_NOT_REWINDED }
        if (delay_days > 0) { fee += this.FEE_DELAY_PER_DAY * delay_days }
        return fee;
    }
}