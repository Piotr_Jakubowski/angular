import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Movie } from '../details/app.movie';
import { Status } from '../details/app.movie.status';
import { MovieService } from '../../movies/app.movie.service';

@Component({
    selector: 'movie-add',
    providers: [MovieService],
    templateUrl: './movie-add.html',
})
export class MovieAddComponent implements OnInit {
    constructor(private movieService: MovieService) { }

    @Output() addAction = new EventEmitter();
    categories: string[];

    ngOnInit() {
        this.movieService.getCategories().subscribe(categories => this.categories = categories);
    }

    addMovie(title: string, director: string, category: string, release_date: Date) {
        let newMovie = new Movie(0, title, director, category, release_date, Status.FREE);
        this.movieService.add(newMovie);
        this.addAction.emit();
    }
}