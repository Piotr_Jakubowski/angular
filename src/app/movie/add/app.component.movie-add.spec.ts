import { MovieAddComponent } from './app.component.movie-add';
import { MovieService } from "../../movies/app.movie.service";
import { Status } from "../../movie/details/app.movie.status";
import { Movie } from "../../movie/details/app.movie";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('MovieAddComponent', function () {
    let de: DebugElement;
    let comp: MovieAddComponent;
    let fixture: ComponentFixture<MovieAddComponent>;
    let movieService: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieAddComponent,
            ],
            imports: [FormsModule, HttpModule],
            providers: [MovieService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieAddComponent);
        comp = fixture.componentInstance;
        movieService = fixture.debugElement.injector.get(MovieService);
    });

    it('should assign categories on init', () => {
        let cat = giveCategories();
        spyOn(movieService, 'getCategories').and.returnValue(Observable.of(cat));
        fixture.detectChanges();

        expect(comp.categories.length).toEqual(cat.length);
    });

    it('should add movie', () => {
        let movie = giveMovie();
        spyOn(movieService, 'add');
        comp.addMovie(movie.title, movie.director, movie.category, movie.release_date);

        expect(movieService.add).toHaveBeenCalled();
    });

    var giveCategories = function () {
        return ['Horror', 'Comedy', 'Drama'];
    }

    var giveMovie = function () {
        return new Movie(0, 'Focus', 'Christopher Nolan', giveCategories()[0], new Date('2010-11-16'), Status.FREE);
    }
});