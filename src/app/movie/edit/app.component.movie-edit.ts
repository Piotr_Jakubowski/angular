import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Movie } from '../details/app.movie';
import { Status } from '../details/app.movie.status';
import { MovieService } from '../../movies/app.movie.service';

@Component({
    selector: 'movie-edit',
    templateUrl: './movie-edit.html',
})
export class MovieEditComponent implements OnInit {

    constructor(private movieService: MovieService) { }

    @Input() movie: Movie;
    @Output() editAction = new EventEmitter();

    movieToUpdate: Movie;

    categories: string[];

    ngOnInit() {
        this.movieService.getCategories().subscribe(categories => this.categories = categories);
        this.movieToUpdate = Object.assign({}, this.movie);
    }

    editMovie(movie: Movie) {
        this.movieService.edit(movie);
        this.editAction.emit();
    }
}