import { MovieEditComponent } from './app.component.movie-edit';
import { MovieService } from "../../movies/app.movie.service";
import { Status } from "../../movie/details/app.movie.status";
import { Movie } from "../../movie/details/app.movie";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('MovieEditComponent', function () {
    let de: DebugElement;
    let comp: MovieEditComponent;
    let fixture: ComponentFixture<MovieEditComponent>;
    let movieService: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieEditComponent,
            ],
            imports: [FormsModule, HttpModule],
            providers: [MovieService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieEditComponent);
        comp = fixture.componentInstance;
        comp.movie = giveMovie();
        movieService = fixture.debugElement.injector.get(MovieService);
    });

    it('should assign categories on init', () => {
        let cat = giveCategories();
        spyOn(movieService, 'getCategories').and.returnValue(Observable.of(cat));
        comp.ngOnInit();
        fixture.detectChanges();

        expect(comp.categories.length).toEqual(cat.length);
    });

    it('should assign movie to update on init', () => {
        comp.ngOnInit();
        fixture.detectChanges();
        let movieToUpdate = comp.movieToUpdate;
        let movie = comp.movie;

        expect(movieToUpdate).toBeDefined();
        expect(movieToUpdate.id).toEqual(movie.id);
        expect(movieToUpdate.title).toEqual(movie.title);
    });

    var giveCategories = function () {
        return ['Horror', 'Comedy', 'Drama'];
    }

    var giveMovie = function () {
        return new Movie(0, 'Focus', 'Christopher Nolan', giveCategories()[0], new Date('2010-11-16'), Status.FREE);
    }
});