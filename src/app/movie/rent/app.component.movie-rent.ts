import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../details/app.movie';
import { MovieService } from '../../movies/app.movie.service';

@Component({
    selector: 'movie-rent',
    templateUrl: './movie-rent.html',
})
export class MovieRentComponent {

    constructor(private movieService: MovieService) { }

    @Input() movie: Movie;

    @Output() rentAction = new EventEmitter();

    rentMovie(id: number, rent_date: Date) {
        this.movieService.rent(id, rent_date);
        this.rentAction.emit();
    }
}