import { MovieRentComponent } from './app.component.movie-rent';
import { MovieService } from "../../movies/app.movie.service";
import { Status } from "../../movie/details/app.movie.status";
import { Movie } from "../../movie/details/app.movie";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('MovieRentComponent', function () {
    let de: DebugElement;
    let comp: MovieRentComponent;
    let fixture: ComponentFixture<MovieRentComponent>;
    let movieService: MovieService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieRentComponent,
            ],
            imports: [FormsModule, HttpModule],
            providers: [MovieService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieRentComponent);
        comp = fixture.componentInstance;
        movieService = fixture.debugElement.injector.get(MovieService);
    });

    it('should rent movie', () => {
        let spy = spyOn(movieService, 'rent');
        comp.rentMovie(1, new Date());
        fixture.detectChanges();

        expect(spy).toHaveBeenCalled();
    });
});