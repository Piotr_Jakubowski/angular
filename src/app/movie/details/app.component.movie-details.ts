import { Component, Input } from '@angular/core';
import { Movie } from './app.movie';

@Component({
    selector: 'movie',
    templateUrl: './movie.html',
})
export class MovieComponent {

    @Input() movie: Movie;

}