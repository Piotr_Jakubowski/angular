import { Status } from './app.movie.status';

export class Movie {
    constructor(
        public id: number,
        public title: string,
        public director: string,
        public category: string,
        public release_date: Date,
        public status: Status,
        public rent_date?: Date
    ) { }

    public poster_url: string;
}