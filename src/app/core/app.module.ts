import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { MovieListComponent } from '../movies/app.component.movie-list';
import { MovieComponent } from '../movie/details/app.component.movie-details';
import { MovieAddComponent } from '../movie/add/app.component.movie-add';
import { MovieEditComponent } from '../movie/edit/app.component.movie-edit';
import { MovieRentComponent } from '../movie/rent/app.component.movie-rent';
import { MovieReturnComponent } from '../movie/return/app.component.movie-return';
import { CustomerListComponent } from '../customers/app.component.customer-list';
import { TitleFilterPipe } from '../movies/app.title-filter.pipe';
import { ArraySortPipe } from '../movies/app.text-sort.pipe';
import { RentedMovieListComponent } from '../movies/rented/app.component.rented-movie-list';
import { AppComponent } from './app.component';

export const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full'
  },
  { path: 'movies', component: MovieListComponent },
  { path: 'customers', component: CustomerListComponent },
  { path: 'rented_movies', component: RentedMovieListComponent },
  { path: '**', component: MovieListComponent }
];

@NgModule({
  imports: [BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    JsonpModule],
  declarations: [
    AppComponent,
    MovieListComponent,
    MovieComponent,
    MovieAddComponent,
    MovieEditComponent,
    MovieRentComponent,
    MovieReturnComponent,
    CustomerListComponent,
    RentedMovieListComponent,
    TitleFilterPipe,
    ArraySortPipe,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
